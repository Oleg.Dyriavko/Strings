package com.home;

public class Main {

    public static void main(String[] args) {

// Work with strings
        String string = "Hello world!";
        System.out.println(string);

//concat
        String string2 = string +" Hello world!";
        System.out.println(string2);

//replace
        String string3 = string2.replace("o","u");
        System.out.println(string3);

//substring
        String substring = string3.substring(0,5);
        String substring2 = string3.substring(6,11);
        System.out.println(substring);
        System.out.println(substring2);

//split
        String string4 =
                "Single Responsibility Principle (Принцип единственной обязанности)\n" +
                "Open Closed Principle (Принцип открытости/закрытости)\n" +
                "Liskov’s Substitution Principle (Принцип подстановки Барбары Лисков)\n" +
                "Interface Segregation Principle (Принцип разделения интерфейса)\n" +
                "Dependency Inversion Principle (Принцип инверсии зависимостей)";

        String [] strings = string4.split(" ");
        for (int i = 0; i<strings.length; i++) {
            System.out.println(strings[i]);
        }
    }
}
